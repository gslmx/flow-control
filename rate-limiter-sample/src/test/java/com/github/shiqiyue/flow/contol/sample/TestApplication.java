package com.github.shiqiyue.flow.contol.sample;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.atomic.AtomicInteger;

import org.junit.Test;
import org.springframework.web.client.RestTemplate;

public class TestApplication {
	
	private RestTemplate restTemplate = new RestTemplate();
	
	@Test
	public void test() {
		AtomicInteger successCount = new AtomicInteger(0);
		int num = 15;
		CountDownLatch countDownLatch = new CountDownLatch(num);
		for (int i = 0; i < num; i++) {
			Thread t = new Thread(() -> {
				String responseMes = restTemplate.getForObject("http://localhost/test/f1/s1", String.class);
				if (responseMes.equals("test1")) {
					successCount.incrementAndGet();
				}
				countDownLatch.countDown();
			});
			t.start();
		}
		try {
			countDownLatch.await();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		System.out.println(successCount.get());
		
	}
	
	@Test
	public void test2() {
		AtomicInteger successCount = new AtomicInteger(0);
		int num = 100;
		CountDownLatch countDownLatch = new CountDownLatch(num);
		for (int i = 0; i < num; i++) {
			Thread t = new Thread(() -> {
				String responseMes = restTemplate.getForObject("http://localhost/test/f1", String.class);
				if (responseMes.equals("test1")) {
					successCount.incrementAndGet();
				}
				countDownLatch.countDown();
			});
			t.start();
		}
		try {
			countDownLatch.await();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		System.out.println(successCount.get());
		
	}
	
}
